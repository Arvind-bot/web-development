const express=require("express");
const bodyParser=require("body-parser");
const app=express();
app.use(bodyParser.urlencoded({extended:true}));
app.get("/",function(req,res){
    res.sendFile(__dirname+"/index.html");
});

app.post("/",function(req,res){
    const body=req.body;
    // console.log(body);
    res.send("Sum is "+ (Number(body.num1)+Number(body.num2)));
});

app.get("/bmicalculator",function(req,res){
    res.sendFile(__dirname+"/bmiCalculator.html");
});

app.post("/bmicalculator",function(req,res){
    const body=req.body;
    res.send("Your BMI is "+Math.round(Number(body.weight)/(Math.pow(Number(body.height),2))));
});

app.listen(3000);