const express=require("express");
const bodyParser=require("body-parser");
const app=express();
const https=require("https");
app.use(bodyParser.urlencoded({extended:true}));
// app.get("/",function(req, res){
//     const url="https://api.openweathermap.org/data/2.5/weather?q=hyderabad&appid=bf09db3f8cea66cebcd0218c461366e6&units=metric";
//     https.get(url,function(response){
//         response.on("data",function(data){
//             const d=JSON.parse(data);
//             const temp=d.main.temp;
//             const wd=d.weather[0].description;
//             const pn=d.name;
//             const iconId=d.weather[0].icon;
//             const imgUrl="https://openweathermap.org/img/wn/"+iconId+"@2x.png";
//             res.write("<p>The weather is currently "+wd+"</p>");
//             res.write("<h1>The temperature in "+ pn +" is "+ temp +" degree Celcius</h1>");
//             res.write("<img src=\""+imgUrl+"\" style=\"background-color:grey\"></img>");
//             res.send();
//         });
//     });
// });

app.get("/",function(req,res){
    res.sendFile(__dirname+"/index.html");
})

app.post("/",function(req,res){
    const rct=req.body.cityName;
    const url="https://api.openweathermap.org/data/2.5/weather?q="+rct+"&appid=bf09db3f8cea66cebcd0218c461366e6&units=metric";
    https.get(url,function(response){
        response.on("data",function(data){
            var d=JSON.parse(data);
            var temp=d.main.temp;
            var wd=d.weather[0].description;
            var pn=d.name;
            var iconId=d.weather[0].icon;
            
            var imgUrl="https://openweathermap.org/img/wn/"+iconId+"@2x.png";
            res.write("<p>The weather is currently "+wd+"</p>");
            res.write("<h1>The temperature in "+ pn +" is "+ temp +" degree Celcius</h1>");
            res.write("<img src=\""+imgUrl+"\" style=\"background-color:grey\"></img>");
            res.send();
        });
    });
    // console.log("post request received");
    
})

app.listen(3000);