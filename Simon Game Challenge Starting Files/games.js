var buttonColors=["green","red","yellow","blue"];
var gamePattern=[];
var gamePatternCounter=0;
var igr=false;
var levelCounter=0;
var randomColorNumber=0;


function getRandomNumber(){
    return Math.floor(Math.random()*4);
}

function playAudio(color){
    var a=new Audio("sounds/"+color+".mp3");
    a.play();
}

function set(color){
    $("#level-title").text("Level " + ++levelCounter);
    $("#"+color).fadeOut(100).fadeIn(100);
}

$("body").on("keypress",function(){
    if(igr===false){
        igr=true;
        randomColorNumber=getRandomNumber();
        var color=buttonColors[randomColorNumber];
        gamePattern.push(color);
        playAudio(color);
        set(color);
    }
});

$(".btn").click(function(event){
    if(igr===true){
        var rid=event.currentTarget.id;
        if(gamePattern[gamePatternCounter++]===rid){
            $("#"+rid).addClass("pressed");
            $("#"+rid).fadeOut(100).fadeIn(100);
            playAudio(rid);
            setTimeout(function(){
                $("#"+rid).removeClass("pressed");  
            },100)
        }else{
            $("body").addClass("game-over");
            $("#"+rid).addClass("pressed");
            $("body").fadeOut(100).fadeIn(100);
            playAudio("wrong");
            setTimeout(function(){
                $("body").removeClass("game-over");
                $("#"+rid).removeClass("pressed");  
            },100)
            gamePattern=[];
            gamePatternCounter=0;
            $("#level-title").text("Game-Over press any key to continue");
            levelCounter=0;
            igr=false;
            return;
            // $("body").removeClass("game-over");
            // $("#"+rid).removeClass("pressed");
        }
        
        if(gamePatternCounter===gamePattern.length){
            randomColorNumber=getRandomNumber();
            var color=buttonColors[randomColorNumber];
            setTimeout(function(){
                gamePattern.push(color);
                set(color);
                playAudio(color);
                gamePatternCounter=0;
            },1000);
            
        }
    }
});

