const mongoose=require("mongoose");
mongoose.connect("mongodb://localhost:27017/fruitsDB",{useNewUrlParser:true});
const fruitSchema=new mongoose.Schema({
    name:String,
    rating:Number,
    review:String
});

const Fruit=mongoose.model("Fruit",fruitSchema);
const fruit=new Fruit({
    name:"Apple",
    rating:7,
    review:"Pretty soilid as a fruiy."
})
// fruit.save();


const personSchema=new mongoose.Schema({
    name:String,
    age:Number
});

const Person=mongoose.model("Person",personSchema);
const person=new Person({
    name: "jhon",
    age:37
});

// person.save();

const kiwi=new Fruit({
    name:"Kiwi",
    rating:10,
    review:"The best fruit!"
})

const orange=new Fruit({
    name:"Orange",
    rating:4,
    review:"Too sour."
})

const banana=new Fruit({
    name:"Apple",
    rating:7,
    review:"Pretty soilid as a fruiy."
})

// Fruit.insertMany([kiwi,orange,banana],function(err){
//     if(err){
//         console.log(err);
//     }else{
//         console.log("successfully inserted");
//     }
// });

Fruit.find(function(err,fruits){
    if(err){
        console.log(err);
    }else{
        mongoose.connection.close();
        fruits.forEach(function(fruit){
            console.log(fruit.name);
        });
        
    }
});

const findDocuments = function(db, callback) {
    // Get the documents collection
    const collection = db.collection('fruits');
    // Find some documents
    collection.find({}).toArray(function(err, fruits) {
      assert.equal(err, null);
      console.log("Found the following records");
      console.log(fruits)
      callback(fruits);
    });
  }
