var l=document.querySelectorAll(".drum").length
function playAudio(char){
    switch(char){
        case "w":
            var b=new Audio("sounds/tom-1.mp3");
            b.play();
        break;
        
        case "a":
            var b=new Audio("sounds/tom-2.mp3");
            b.play();
        break;
        
        case "s":
            var b=new Audio("sounds/tom-3.mp3");
            b.play();
        break;
        
        case "d":
            var b=new Audio("sounds/tom-4.mp3");
            b.play();
        break;

        case "j":
            var b=new Audio("sounds/snare.mp3");
            b.play();
        break;

        case "k":
            var b=new Audio("sounds/crash.mp3");
            b.play();
        break;

        case "l":
            var b=new Audio("sounds/kick-bass.mp3");
            b.play();
        break;

        default:
            console.log(char);
    }
}

for(var i=0;i<l;i++){
    document.querySelectorAll(".drum")[i].addEventListener("click",function (){
        var bp=this.innerHTML;
        playAudio(bp);
        buttonAnimation(bp);
    });
}

document.addEventListener("keydown",function(event){
    var bp=event.key;
    playAudio(bp);
    buttonAnimation(bp);
});

function buttonAnimation(char){
    if(char!=" "){
        var e=document.querySelector("."+char);
        if(e!=null){
            e.classList.add("pressed");
            setTimeout(function(){
                e.classList.remove("pressed");
            },100);
        }else{
            console.log(char);
        }
    }
}

